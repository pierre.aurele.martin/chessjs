class Piece {
  toString() {
    return this.symbol;
  }
}

class King extends Piece {
  constructor() {
    this.symbol = "♚"
  }
}

class Game {
  start() { /* Starts game logic etc. */ }
  getPieceAt(position) { /* Returns a piece on board */ }

  async move(fromPosition, toPosition) {
    try {
      const newBoardState = await this.makeMoveRequest(fromPosition, toPosition);
      this.setBoardState(newBoardState);
      // TODO
    } catch (e) {
      this.handleError(e);
    }
  }

  onMove(handler) {
    // TODO
  }
}

const game = new Game();

game.onMove((move) => {
  console.log('moved', move.piece, move.from, move.to);
});

game.start();
